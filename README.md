# MusterMasterThesis

A template of master's thesis at Agkuhr group according to the LMU rules

Get started:
1. Go to MusterLatex repository
2. Download the zipped file with the symbol beside 'Clone'
3. Import in your Latex Editor e.g Overleaf: 'New Project' -> 'Upload Project' -> 'Select a.zip file'
4. Start with 'diss.tex' (the main tex file) and enjoy your writing!
